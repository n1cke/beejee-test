import { connect } from 'react-redux'
import TaskList from '../components/TaskList'
import {
    getTaskList,
    getTaskEditStatus,
    getTaskEditText,
    editTaskText
} from '../actions'

const mapStateToProps = state => ({
    tasks: state.tasks,
    page: state.loader.page,
    sort_field: state.loader.sort_field,
    sort_direction: state.loader.sort_direction,
    isAuthorized: state.account.isAuthorized,
    token: state.account.token,
    message: state.taskedit.message,
    newtext: state.taskedit.text,
    loginMessage: state.account.loginMessage
})

const mapDispatchToProps = {
    getTaskList,
    getTaskEditStatus,
    getTaskEditText,
    editTaskText
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskList)
