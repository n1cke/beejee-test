import { connect } from 'react-redux'
import Sorter from '../components/Sorter'
import { setSortField, setSortDirection } from '../actions'

const mapStateToProps = state => ({
    sort_field: state.loader.sort_field,
    sort_direction: state.loader.sort_direction
})

const mapDispatchToProps = {
    setSortField,
    setSortDirection
}

export default connect(mapStateToProps, mapDispatchToProps)(Sorter)
