import { connect } from 'react-redux'
import Account from '../components/Account'
import { setUsername, setPassword, getLogin, exitLogin } from '../actions'

const mapStateToProps = state => ({
    isAuthorized: state.account.isAuthorized,
    username: state.account.username,
    password: state.account.password,
    message: state.account.message
})

const mapDispatchToProps = {
    setPassword,
    setUsername,
    getLogin,
    exitLogin
}

export default connect(mapStateToProps, mapDispatchToProps)(Account)
