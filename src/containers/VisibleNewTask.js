import { connect } from 'react-redux'
import NewTask from '../components/NewTask'
import {
    setTaskUsername,
    setTaskEmail,
    setTaskText,
    getTaskCreate
} from '../actions'

const mapStateToProps = state => ({
    username: state.newtask.username,
    email: state.newtask.email,
    text: state.newtask.text,
    message: state.newtask.message,
    successText: state.newtask.successText
})

const mapDispatchToProps = {
    setTaskUsername,
    setTaskEmail,
    setTaskText,
    getTaskCreate
}

export default connect(mapStateToProps, mapDispatchToProps)(NewTask)
