import { connect } from 'react-redux'
import Paginator from '../components/Paginator'
import { getTaskList } from '../actions'

const mapStateToProps = state => ({
    page: state.loader.page,
    pageCount: state.loader.pageCount
})

const mapDispatchToProps = {
    getTaskList
}

export default connect(mapStateToProps, mapDispatchToProps)(Paginator)
