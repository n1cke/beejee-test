import { sortFields, sortDirections } from '../actions'

const initial = {
    isLoading: true,
    page: 0,
    pageCount: 0,
    sort_field: sortFields['по ID задачи'],
    sort_direction: sortDirections['по убыванию']
}

const loader = (state = initial, action) => {
    switch (action.type) {
        case 'REQUEST_TASK_LIST':
            return { ...state, isLoading: true }
        case 'INVALID_TASK_LIST':
            return { ...state, isLoading: false }
        case 'FAIL_TASK_LIST':
            return { ...state, isLoading: false }
        case 'RECEIVE_TASK_LIST':
            return {
                ...state,
                page: action.page,
                pageCount: Math.floor(action.total_task_count / 3),
                isLoading: false
            }
        case 'SET_SORT_FIELD':
            return { ...state, sort_field: action.sort_field }
        case 'SET_SORT_DIRECTION':
            return { ...state, sort_direction: action.sort_direction }
        default:
            return state
    }
}

export default loader
