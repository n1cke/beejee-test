const initial = {
    username: '',
    email: '',
    text: '',
    message: {},
    successText: ''
}

const newtask = (state = initial, action) => {
    switch (action.type) {
        case 'SET_TASK_USERNAME':
            return { ...state, username: action.username }
        case 'SET_TASK_EMAIL':
            return { ...state, email: action.email }
        case 'SET_TASK_TEXT':
            return { ...state, text: action.text }
        case 'REQUEST_TASK_CREATE':
            return { ...state }
        case 'INVALID_TASK_CREATE':
            return { ...state, message: action.message }
        case 'FAIL_TASK_CREATE':
            return { ...state, message: action.message }
        case 'RECEIVE_TASK_CREATE':
            return {
                username: '',
                email: '',
                text: '',
                message: {},
                successText: 'Задача успешно создана!'
            }
        case 'REMOVE_CREATE_MESSAGE':
            return {
                ...state,
                successText: ''
            }
        default:
            return state
    }
}

export default newtask
