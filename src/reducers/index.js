import { combineReducers } from 'redux'
import tasks from './tasks'
import loader from './loader'
import account from './account'
import newtask from './newtask'
import taskedit from './taskedit'

export default combineReducers({
    tasks,
    loader,
    account,
    newtask,
    taskedit
})
