const initial = {
    message: {},
    text: ''
}

const taskedit = (state = initial, action) => {
    switch (action.type) {
        case 'EDIT_TASK_TEXT':
            return { ...state, message: {}, text: action.text }
        case 'REQUEST_TASK_EDIT_TEXT':
            return { ...state, message: {} }
        case 'INVALID_TASK_EDIT_TEXT':
            return { ...state, message: action.message }
        case 'FAIL_TASK_EDIT_TEXT':
            return { ...state, message: action.message }
        case 'RECEIVE_TASK_EDIT_TEXT':
            return {
                ...state
            }
        default:
            return state
    }
}

export default taskedit
