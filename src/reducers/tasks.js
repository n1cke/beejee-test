const tasks = (state = [], action) => {
    switch (action.type) {
        case 'RECEIVE_TASK_EDIT_STATUS':
            return [
                ...state.map(task => {
                    if (task.id === action.id) {
                        return { ...task, status: action.status }
                    } else {
                        return task
                    }
                })
            ]
        case 'RECEIVE_TASK_EDIT_TEXT':
            return [
                ...state.map(task => {
                    if (task.id === action.id) {
                        return {
                            ...task,
                            text: action.text,
                            status: action.status
                        }
                    } else {
                        return task
                    }
                })
            ]
        case 'RECEIVE_TASK_LIST':
            return [...action.tasks]
        default:
            return state
    }
}

export default tasks
