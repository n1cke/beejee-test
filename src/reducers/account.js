const initial = {
    username: '',
    password: '',
    token: '',
    isAuthorized: false,
    message: {},
    loginMessage: ''
}

const account = (state = initial, action) => {
    switch (action.type) {
        case 'SET_USERNAME':
            return { ...state, username: action.username }
        case 'SET_PASSWORD':
            return { ...state, password: action.password }
        case 'REQUEST_LOGIN':
            return { ...state }
        case 'INVALID_LOGIN':
            return { ...state, message: action.message }
        case 'FAIL_LOGIN':
            return { ...state, message: action.message }
        case 'RECEIVE_LOGIN':
            return {
                ...state,
                token: action.token,
                username: action.username,
                isAuthorized: action.token && action.username ? true : false
            }
        case 'EXIT_LOGIN':
            return {
                ...state,
                username: '',
                password: '',
                token: '',
                isAuthorized: false,
                message: {}
            }
        case 'CREATE_LOGIN_MESSAGE':
            return {
                ...state,
                loginMessage: 'Вы не авторизованы для редактирования!'
            }
        case 'REMOVE_LOGIN_MESSAGE':
            return {
                ...state,
                loginMessage: ''
            }
        default:
            return state
    }
}

export default account
