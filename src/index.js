import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import * as serviceWorker from './serviceWorker'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import ReduxThunk from 'redux-thunk'
import rootReducer from './reducers'
import { updateCookies } from './actions'

import 'antd/dist/antd.css'
import './index.css'

const middleware = [applyMiddleware(ReduxThunk)]
if (window.__REDUX_DEVTOOLS_EXTENSION__)
    middleware.push(window.__REDUX_DEVTOOLS_EXTENSION__())

const store = createStore(rootReducer, compose(...middleware))

updateCookies(store.dispatch)

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
