import cookies from 'browser-cookies'

export const REQUEST_TASK_LIST = 'REQUEST_TASK_LIST'
export const INVALID_TASK_LIST = 'INVALID_TASK_LIST'
export const FAIL_TASK_LIST = 'FAIL_TASK_LIST'
export const RECEIVE_TASK_LIST = 'RECEIVE_TASK_LIST'

export const REQUEST_TASK_CREATE = 'REQUEST_TASK_CREATE'
export const INVALID_TASK_CREATE = 'INVALID_TASK_CREATE'
export const FAIL_TASK_CREATE = 'FAIL_TASK_CREATE'
export const RECEIVE_TASK_CREATE = 'RECEIVE_TASK_CREATE'
export const REMOVE_CREATE_MESSAGE = 'REMOVE_CREATE_MESSAGE'

export const REQUEST_LOGIN = 'REQUEST_LOGIN'
export const INVALID_LOGIN = 'INVALID_LOGIN'
export const FAIL_LOGIN = 'FAIL_LOGIN'
export const RECEIVE_LOGIN = 'RECEIVE_LOGIN'
export const EXIT_LOGIN = 'EXIT_LOGIN'
export const CREATE_LOGIN_MESSAGE = 'CREATE_LOGIN_MESSAGE'
export const REMOVE_LOGIN_MESSAGE = 'REMOVE_LOGIN_MESSAGE'

export const REQUEST_TASK_EDIT_TEXT = 'REQUEST_TASK_EDIT_TEXT'
export const INVALID_TASK_EDIT_TEXT = 'INVALID_TASK_EDIT_TEXT'
export const FAIL_TASK_EDIT_TEXT = 'FAIL_TASK_EDIT_TEXT'
export const RECEIVE_TASK_EDIT_TEXT = 'RECEIVE_TASK_EDIT_TEXT'

export const REQUEST_TASK_EDIT_STATUS = 'REQUEST_TASK_EDIT_STATUS'
export const INVALID_TASK_EDIT_STATUS = 'INVALID_TASK_EDIT_STATUS'
export const FAIL_TASK_EDIT_STATUS = 'FAIL_TASK_EDIT_STATUS'
export const RECEIVE_TASK_EDIT_STATUS = 'RECEIVE_TASK_EDIT_STATUS'

export const SET_SORT_FIELD = 'SET_SORT_FIELD'
export const SET_SORT_DIRECTION = 'SET_SORT_DIRECTION'

export const SET_PASSWORD = 'SET_PASSWORD'
export const SET_USERNAME = 'SET_USERNAME'

export const SET_TASK_USERNAME = 'SET_TASK_USERNAME'
export const SET_TASK_EMAIL = 'SET_TASK_EMAIL'
export const SET_TASK_TEXT = 'SET_TASK_TEXT'

export const EDIT_TASK_TEXT = 'EDIT_TASK_TEXT'
export const EDIT_TASK_STATUS = 'EDIT_TASK_STATUS'

const baseURL = 'https://uxcandy.com/~shapoval/test-task-backend/v2'
const devInfo = '?developer=Name'

/** @typedef {0 | 1 | 10 | 11} Status */

/** @typedef {{status: 'error', message: string | Object<string, string>} | Error} Err */

/** @typedef {object} Task
 * @prop {string} id
 * @prop {string} username
 * @prop {string} email
 * @prop {string} text
 * @prop {Status} status
 */

export const sortFields = {
    'по ID задачи': 'id',
    'по имени пользователя': 'username',
    'по email пользователя': 'email',
    'по статусу задания': 'status'
}

export const sortDirections = {
    'по возрастанию': 'asc',
    'по убыванию': 'desc'
}

export const setSortField = sort_field => ({
    type: SET_SORT_FIELD,
    sort_field
})

export const setSortDirection = sort_direction => ({
    type: SET_SORT_DIRECTION,
    sort_direction
})

export const setUsername = username => ({
    type: SET_USERNAME,
    username
})

export const setPassword = password => ({
    type: SET_PASSWORD,
    password
})

export const setTaskUsername = username => ({
    type: SET_TASK_USERNAME,
    username
})
export const setTaskEmail = email => ({
    type: SET_TASK_EMAIL,
    email
})
export const setTaskText = text => ({
    type: SET_TASK_TEXT,
    text
})

export const editTaskText = text => ({
    type: EDIT_TASK_TEXT,
    text
})

/**
 * Выход из аккаунта
 */
export function exitLogin() {
    return dispatch => {
        cookies.erase('username')
        cookies.erase('token')
        dispatch({ type: EXIT_LOGIN })
    }
}

/**
 * Сохраняет печеньки
 * @param {string} username
 * @param {string} token
 */
function setCookies(username, token) {
    const ONE_DAY_IN_MS = 24 * 60 * 60 * 1000
    const expires = Date.now() + ONE_DAY_IN_MS
    cookies.set('username', username, { expires })
    cookies.set('token', token, { expires })
}

/**
 * Обновляет печеньки в сторе
 * @param {function} dispatch
 */
export function updateCookies(dispatch) {
    const username = cookies.get('username') || ''
    const token = cookies.get('token') || ''
    dispatch({ type: RECEIVE_LOGIN, username, token })
}

/**
 * Запрос списка заданий
 * @param {object} props
 * @param {'id' | 'username' | 'email' | 'status'} [props.sort_field]
 * @param {'asc' | 'desc'} [props.sort_direction]
 * @param {number} [props.page]
 * @returns {(dispatch: function) => Promise<{status: 'ok', message: {tasks: Array<Task>, total_task_count: string}} | Err>}
 */
export function getTaskList(props) {
    return async dispatch => {
        const { sort_field, sort_direction, page } = props
        let url = `${baseURL}/${devInfo}`
        if (sort_field != null) url += '&sort_field=' + sort_field
        if (sort_direction != null) url += '&sort_direction=' + sort_direction
        if (page != null) url += '&page=' + page
        dispatch({ type: REQUEST_TASK_LIST, ...props })
        try {
            const response = await fetch(url)
            const data = await response.json()
            if (data.status === 'ok') {
                dispatch({
                    type: RECEIVE_TASK_LIST,
                    ...props,
                    tasks: data.message.tasks,
                    total_task_count: data.message.total_task_count
                })
            } else {
                dispatch({
                    type: INVALID_TASK_LIST,
                    ...props,
                    message: data.message
                })
            }
            return data
        } catch (error) {
            dispatch({
                type: FAIL_TASK_LIST,
                ...props,
                message: error.message
            })
            return error
        }
    }
}

/**
 * Запрос создания задания
 * @param {object} props
 * @param {string} props.username
 * @param {string} props.email
 * @param {string} props.text
 * @returns {(dispatch: function) => Promise<{status: 'ok', message: Task} | Err>}
 */
export function getTaskCreate(props) {
    return async dispatch => {
        const { username, email, text } = props
        const url = `${baseURL}/create/${devInfo}`
        const formData = new FormData()
        formData.append('username', username)
        formData.append('email', email)
        formData.append('text', text)
        dispatch({ type: REQUEST_TASK_CREATE, ...props })
        try {
            const response = await fetch(url, {
                method: 'POST',
                body: formData
            })
            const data = await response.json()
            if (data.status === 'ok') {
                dispatch({
                    type: RECEIVE_TASK_CREATE,
                    ...props,
                    task: data.message
                })
                setTimeout(dispatch, 5000, {
                    type: REMOVE_CREATE_MESSAGE
                })
            } else {
                dispatch({
                    type: INVALID_TASK_CREATE,
                    ...props,
                    message: data.message
                })
            }
            return data
        } catch (error) {
            dispatch({
                type: FAIL_TASK_CREATE,
                ...props,
                message: error.message
            })
            return error
        }
    }
}

/**
 * Запрос авторизации
 * @param {object} props
 * @param {string} props.username
 * @param {string} props.password
 * @returns {(dispatch: function) => Promise<{status: 'ok', message: { token: string }} | Err>}
 */
export function getLogin(props) {
    return async dispatch => {
        const { username, password } = props
        const url = `${baseURL}/login/${devInfo}`
        const formData = new FormData()
        formData.append('username', username)
        formData.append('password', password)
        dispatch({ type: REQUEST_LOGIN, ...props })
        try {
            const response = await fetch(url, {
                method: 'POST',
                body: formData
            })
            const data = await response.json()
            if (data.status === 'ok') {
                dispatch({
                    type: RECEIVE_LOGIN,
                    ...props,
                    token: data.message.token
                })
                setCookies(username, data.message.token)
            } else {
                dispatch({
                    type: INVALID_LOGIN,
                    ...props,
                    message: data.message
                })
            }
            return data
        } catch (error) {
            dispatch({
                type: FAIL_LOGIN,
                ...props,
                message: error.message
            })
            return error
        }
    }
}

/**
 * Запрос редактирования задачи
 * @param {object} props
 * @param {string} props.token
 * @param {string} props.id
 * @param {string} props.text
 * @param {Status} props.status
 * @returns {(dispatch: function) => Promise<{status: 'ok'} | Err>}
 */
export function getTaskEditText(props) {
    return async dispatch => {
        const { id, text, status, token } = props
        const url = `${baseURL}/edit/${id}/${devInfo}`
        const formData = new FormData()
        const cookieToken = cookies.get('token') || ''
        if (!cookieToken) {
            dispatch({ type: CREATE_LOGIN_MESSAGE })
            setTimeout(dispatch, 5000, {
                type: REMOVE_LOGIN_MESSAGE
            })
            return dispatch({ type: EXIT_LOGIN })
        }
        formData.append('token', token)
        formData.append('text', text)
        formData.append('status', status.toString())
        dispatch({ type: REQUEST_TASK_EDIT_TEXT, ...props })
        try {
            const response = await fetch(url, {
                method: 'POST',
                body: formData
            })
            const data = await response.json()
            if (data.status === 'ok') {
                dispatch({
                    type: RECEIVE_TASK_EDIT_TEXT,
                    ...props
                })
            } else {
                dispatch({
                    type: INVALID_TASK_EDIT_TEXT,
                    ...props,
                    message: data.message
                })
            }
            return data
        } catch (error) {
            dispatch({
                type: FAIL_TASK_EDIT_TEXT,
                ...props,
                message: error.message
            })
            return error
        }
    }
}

/**
 * Запрос редактирования статуса
 * @param {object} props
 * @param {string} props.token
 * @param {string} props.id
 * @param {Status} props.status
 * @returns {(dispatch: function) => Promise<{status: 'ok'} | Err>}
 */
export function getTaskEditStatus(props) {
    return async dispatch => {
        const { id, status, token } = props
        const cookieToken = cookies.get('token') || ''
        if (!cookieToken) {
            dispatch({ type: CREATE_LOGIN_MESSAGE })
            setTimeout(dispatch, 5000, {
                type: REMOVE_LOGIN_MESSAGE
            })
            return dispatch({ type: EXIT_LOGIN })
        }
        const url = `${baseURL}/edit/${id}/${devInfo}`
        const formData = new FormData()
        formData.append('token', token)
        formData.append('status', status.toString())
        dispatch({ type: REQUEST_TASK_EDIT_STATUS, ...props })
        try {
            const response = await fetch(url, {
                method: 'POST',
                body: formData
            })
            const data = await response.json()
            if (data.status === 'ok') {
                dispatch({
                    type: RECEIVE_TASK_EDIT_STATUS,
                    ...props
                })
            } else {
                dispatch({
                    type: INVALID_TASK_EDIT_STATUS,
                    ...props,
                    message: data.message
                })
            }
            return data
        } catch (error) {
            dispatch({
                type: FAIL_TASK_EDIT_STATUS,
                ...props,
                message: error.message
            })
            return error
        }
    }
}
