import React from 'react'
import VisibleTaskList from '../containers/VisibleTaskList'
import VisiblePaginator from '../containers/VisiblePaginator'
import VisibleSorter from '../containers/VisibleSorter'

const Main = props => (
    <div>
        <VisibleSorter />
        <VisibleTaskList />
        <VisiblePaginator />
    </div>
)

export default Main
