import React from 'react'
import { Select } from 'antd'
import { sortFields, sortDirections } from '../actions'

const sortFieldKeys = Object.keys(sortFields)
const sortDirectionKeys = Object.keys(sortDirections)

const Sorter = ({
    sort_field,
    sort_direction,
    setSortField,
    setSortDirection
}) => {
    return (
        <div className="sorter">
            <Select
                value={sort_field}
                onChange={value => setSortField(value)}
                style={{ width: '100%', marginBottom: 16 }}
            >
                {sortFieldKeys.map(sortField => (
                    <Select.Option
                        key={sortField}
                        value={sortFields[sortField]}
                    >
                        {sortField}
                    </Select.Option>
                ))}
            </Select>
            <Select
                value={sort_direction}
                onChange={value => setSortDirection(value)}
                style={{ width: '100%' }}
            >
                {sortDirectionKeys.map(sortDirection => (
                    <Select.Option
                        key={sortDirection}
                        value={sortDirections[sortDirection]}
                    >
                        {sortDirection}
                    </Select.Option>
                ))}
            </Select>
        </div>
    )
}

export default Sorter
