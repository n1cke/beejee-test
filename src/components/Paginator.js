import React from 'react'
import { Pagination } from 'antd'

const Paginator = ({ page, pageCount, getTaskList }) => (
    <Pagination
        style={{ marginBottom: 16 }}
        current={page || 1}
        total={pageCount}
        pageSize={1}
        onChange={(page, pageSize) => getTaskList({ page: page })}
    />
)

export default Paginator
