import React, { useState } from 'react'
import { Divider, Icon, Button, Modal, Form, Input } from 'antd'

function isTaskDone(status) {
    return (status || 0) >= 10
}

function isTaskEditedByAdmin(status) {
    return (status || 0) % 2 === 1
}

const Task = ({
    id,
    email,
    status,
    text,
    newtext,
    token,
    username,
    isAuthorized,
    setDone,
    setUndone,
    setText,
    editTaskText,
    message
}) => {
    const [state, setState] = useState({ isEditModalVisible: false })

    const newtextError = message.text

    async function onSubmit(e) {
        e.preventDefault()
        setText()
        setState({ isEditModalVisible: false })
    }

    return (
        <div className="task-card">
            <Modal
                title={`Редактирование текста задачи №${id}`}
                visible={state.isEditModalVisible}
                footer={null}
                // onOk={this.handleOk}
                onCancel={() => setState({ isEditModalVisible: false })}
            >
                <Form onSubmit={onSubmit}>
                    <Form.Item
                        validateStatus={newtextError ? 'error' : ''}
                        help={newtextError || ''}
                    >
                        <Input.TextArea
                            autoFocus
                            value={newtext}
                            onChange={e => editTaskText(e.target.value)}
                            placeholder="Текст задачи"
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Обновить текст
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
            <div className="task-card-from">
                Задача №{id} от пользователя {username}
            </div>
            <div className="task-card-email">
                email: <a href={`mailto:${email}`}>{email}</a>
            </div>
            <Divider />
            <div className="task-card-text">{text}</div>
            <Divider />
            <div className="task-card-status">
                <span>
                    {isTaskDone(status) ? (
                        <span>
                            <Icon
                                type="check-circle"
                                style={{ marginRight: 8 }}
                            />
                            выполнена
                        </span>
                    ) : (
                        <span>не выполнена</span>
                    )}
                </span>
            </div>
            {isTaskEditedByAdmin(status) && (
                <div className="task-card-edited">
                    <Icon
                        type="exclamation-circle"
                        style={{ marginRight: 8 }}
                    />
                    отредактировано администратором
                </div>
            )}
            {isAuthorized && (
                <div>
                    <Button
                        icon="edit"
                        style={{ marginTop: 8 }}
                        onClick={() => {
                            editTaskText(text)
                            setState({ isEditModalVisible: true })
                        }}
                    >
                        редактировать текст
                    </Button>
                    <div>
                        {!isTaskDone(status) && (
                            <Button
                                icon="check-circle"
                                style={{ marginTop: 8 }}
                                type="primary"
                                onClick={setDone}
                            >
                                пометить выполненной
                            </Button>
                        )}
                        {isTaskDone(status) && (
                            <Button
                                icon="minus-circle"
                                style={{ marginTop: 8 }}
                                onClick={setUndone}
                            >
                                пометить невыполненной
                            </Button>
                        )}
                    </div>
                </div>
            )}
        </div>
    )
}

export default Task
