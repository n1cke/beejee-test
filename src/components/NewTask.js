import React from 'react'
import { Form, Icon, Input, Button } from 'antd'

const NewTask = props => {
    const {
        message,
        username,
        email,
        text,
        successText,
        setTaskUsername,
        setTaskEmail,
        setTaskText,
        getTaskCreate
    } = props

    async function onSubmit(e) {
        e.preventDefault()
        getTaskCreate({ username, email, text })
    }

    const usernameError = message.username
    const emailError = message.email
    const textError = message.text

    return (
        <div>
            <div>
                <Form onSubmit={onSubmit}>
                    <Form.Item
                        validateStatus={usernameError ? 'error' : ''}
                        help={usernameError || ''}
                    >
                        <Input
                            value={username}
                            onChange={e => setTaskUsername(e.target.value)}
                            prefix={
                                <Icon
                                    type="user"
                                    style={{ color: 'rgba(0,0,0,.25)' }}
                                />
                            }
                            placeholder="Имя пользователя"
                        />
                    </Form.Item>
                    <Form.Item
                        validateStatus={emailError ? 'error' : ''}
                        help={emailError || ''}
                    >
                        <Input
                            value={email}
                            onChange={e => setTaskEmail(e.target.value)}
                            prefix={
                                <Icon
                                    type="mail"
                                    style={{ color: 'rgba(0,0,0,.25)' }}
                                />
                            }
                            type="email"
                            placeholder="Электронная почта"
                        />
                    </Form.Item>
                    <Form.Item
                        validateStatus={textError ? 'error' : ''}
                        help={textError || ''}
                    >
                        <Input.TextArea
                            value={text}
                            onChange={e => setTaskText(e.target.value)}
                            placeholder="Текст задачи"
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Создать задачу
                        </Button>
                    </Form.Item>
                </Form>

                {successText && (
                    <h4
                        style={{
                            border: '1px dashed white',
                            textAlign: 'center',
                            borderRadius: 8,
                            padding: 4,
                            color: 'white',
                            background: 'green'
                        }}
                    >
                        {successText}
                    </h4>
                )}
            </div>
        </div>
    )
}

export default NewTask
