import React from 'react'

import { Form, Icon, Input, Button } from 'antd'

const Account = props => {
    const {
        isAuthorized,
        message,
        username,
        password,
        setUsername,
        setPassword,
        getLogin,
        exitLogin
    } = props

    if (isAuthorized) {
        return (
            <div>
                <h3>Добро пожаловать, {username}!</h3>
                <Button type="danger" onClick={() => exitLogin()}>
                    Выйти из аккаунта
                </Button>
            </div>
        )
    }

    async function onSubmit(e) {
        e.preventDefault()
        getLogin({ username, password })
    }

    const usernameError = message.username
    const passwordError = message.password

    return (
        <div>
            <Form onSubmit={onSubmit}>
                <Form.Item
                    validateStatus={usernameError ? 'error' : ''}
                    help={usernameError || ''}
                >
                    <Input
                        value={username}
                        onChange={e => setUsername(e.target.value)}
                        prefix={
                            <Icon
                                type="user"
                                style={{ color: 'rgba(0,0,0,.25)' }}
                            />
                        }
                        placeholder="Имя пользователя"
                    />
                </Form.Item>
                <Form.Item
                    validateStatus={passwordError ? 'error' : ''}
                    help={passwordError || ''}
                >
                    <Input
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        prefix={
                            <Icon
                                type="lock"
                                style={{ color: 'rgba(0,0,0,.25)' }}
                            />
                        }
                        type="password"
                        placeholder="Пароль"
                        autoComplete="password"
                    />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Войти
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}

export default Account
