import React, { useEffect } from 'react'
import Task from './Task'

function isTaskEditedByAdmin(status) {
    return (status || 0) % 2 === 1
}

let oldPage = -1

const TaskList = ({
    tasks,
    page,
    sort_field,
    sort_direction,
    isAuthorized,
    loginMessage,
    token,
    newtext,
    message,
    getTaskList,
    getTaskEditStatus,
    getTaskEditText,
    editTaskText
}) => {
    useEffect(() => {
        getTaskList({ page, sort_field, sort_direction })
    }, [getTaskList, page, sort_field, sort_direction])

    useEffect(() => {
        if (oldPage !== page) {
            window.scrollTo({
                top: 0,
                behavior: 'smooth'
            })
            oldPage = page
        }
    })

    return (
        <div>
            {loginMessage && (
                <h4
                    style={{
                        border: '1px dashed white',
                        textAlign: 'center',
                        borderRadius: 8,
                        padding: 4,
                        marginTop: 16,
                        color: 'white',
                        background: 'red'
                    }}
                >
                    {loginMessage}
                </h4>
            )}
            {tasks.map(task => (
                <Task
                    key={task.id}
                    {...task}
                    isAuthorized={isAuthorized}
                    newtext={newtext}
                    message={message}
                    token={token}
                    editTaskText={editTaskText}
                    setText={() =>
                        getTaskEditText({
                            token,
                            id: task.id,
                            text: newtext,
                            status: isTaskEditedByAdmin(task.status)
                                ? task.status
                                : task.status + 1
                        })
                    }
                    setUndone={() =>
                        getTaskEditStatus({
                            token,
                            id: task.id,
                            status: isTaskEditedByAdmin(task.status) ? 1 : 0
                        })
                    }
                    setDone={() =>
                        getTaskEditStatus({
                            token,
                            id: task.id,
                            status: isTaskEditedByAdmin(task.status) ? 11 : 10
                        })
                    }
                />
            ))}
        </div>
    )
}

export default TaskList
