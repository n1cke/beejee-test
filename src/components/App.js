import React from 'react'

import Main from './Main'
import VisibleAccount from '../containers/VisibleAccount'
import VisibleNewTask from './../containers/VisibleNewTask'
import { PageHeader } from 'antd'
import { Menu } from 'antd'

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'

const App = () => {
    return (
        <div>
            <Router>
                <div>
                    <Menu
                        mode="horizontal"
                        theme="dark"
                        style={{ marginBottom: 16 }}
                    >
                        <Menu.Item key="main">
                            <Link to="/">Задачи</Link>
                        </Menu.Item>
                        <Menu.Item key="newtask">
                            <Link to="/newtask">Новая</Link>
                        </Menu.Item>
                        <Menu.Item key="login">
                            <Link to="/login">Аккаунт</Link>
                        </Menu.Item>
                    </Menu>
                    <main>
                        <Switch>
                            <Route path="/login">
                                <PageHeader title="Ваш аккаунт" />
                                <VisibleAccount />
                            </Route>
                            <Route path="/newtask">
                                <PageHeader title="Новая задача" />
                                <VisibleNewTask />
                            </Route>
                            <Route path="/">
                                <PageHeader title="Все задачи" />
                                <Main />
                            </Route>
                        </Switch>
                    </main>
                </div>
            </Router>
        </div>
    )
}

export default App
